# README #

This is the README for my "K. Veen Code Library" repo.

### What is this repository for? ###

* This repo is for show cassing all my code.
* V1.0

### Description ###

* The main purpose of this repo is to show case my skills to interested parties.
* The repo includes code i have written for personal use or code i have had to do for school projects.

### Contact ###

* K. Veen
* Linkedin: https://www.linkedin.com/in/kevin-veen/