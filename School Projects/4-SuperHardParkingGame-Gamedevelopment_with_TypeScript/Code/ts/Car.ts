/// <reference path="GameObject.ts" />

class Car extends GameObject {

    // attributes


    // constructor
    constructor(name: string, xPosition: number = 0, yPosition: number = 0) {
        super(name, xPosition, yPosition);
    }

    // methodes

    // moves the car by the given amount of pixels
    public move(xPosition: number):void{
        this._xPos += xPosition;
    }
}