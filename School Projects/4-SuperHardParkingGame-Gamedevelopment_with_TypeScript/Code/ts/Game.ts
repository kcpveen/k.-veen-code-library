class Game {

    // attributes
    private _element: HTMLElement;
    private _car: Map<String, Car>;
    private _parkingSpace: ParkingSpace;
    private _scoreboard: ScoreBoard;
    private _carMoveRate: number;
    private _currentTurn: number; //can also be read as current player.
    private _wall: Wall;
    private _stopGame: Number;
    private _collision: Boolean;

    // constructor
    constructor() {
        // create objects
        this._car = new Map();
        this._car.set("carOrange", new Car("carOrange"));
        this._car.set("carWhite", new Car("carWhite"));
        this._parkingSpace = new ParkingSpace("parkingLot");
        this._scoreboard = new ScoreBoard("scoreBoard");
        this._wall = new Wall("wallLong");

        // sets attributes
        this._element = document.getElementById('container');
        this._carMoveRate = 0;
        this._currentTurn = 0;
        this._stopGame = 0;
        this._collision = false;

        // initialises the game menu
        this.gameMenu();
    }

    // methodes

    // Detects collision between car,parkingLot and wall
    private collision(): void {
        // set rectangles around objects
        var carRect: any;
        const parkingRect = document.getElementById('parkingLot').getBoundingClientRect();
        const wallRect = document.getElementById('wallLong').getBoundingClientRect();
        
        // set rectangle for the current car
        if (this._currentTurn == 0) {
            carRect = document.getElementById('carOrange').getBoundingClientRect();
        }
        if (this._currentTurn == 1) {
            carRect = document.getElementById('carWhite').getBoundingClientRect();
        }

        // collision detection for the parkingLot
        if (carRect.right <= parkingRect.right && carRect.left >= parkingRect.left) {
            if (this._currentTurn == 0) {
                this._scoreboard.increaseScoreOf(this._currentTurn);
                this.changePlayer();
            }
            else if (this._currentTurn == 1) {
                this._scoreboard.increaseScoreOf(this._currentTurn);
                this.changePlayer();
            }
            this.update();
            console.log('Parked');
        }
        else {
            console.log('Not parked');
        }

        // collision detection for the wall
        if (carRect.right >= wallRect.left) {
            if (this._currentTurn == 0) {
                this.changePlayer();
            }
            else if (this._currentTurn == 1) {
                this.changePlayer();
            }
            this.update();
            console.log('Wall collision');
        }
        else {
            console.log('No wall collision');
        }
    }

    // Draws the game elements
    private draw(): void {
        this._parkingSpace.draw(this._element);
        this._scoreboard.draw(this._element);
        this.highlightPlayer();
        this._wall.draw(this._element);

        // draws the current player car
        if (this._currentTurn == 0) {
            this._car.get("carOrange").draw(this._element);
        }
        if (this._currentTurn == 1) {
            this._car.get("carWhite").draw(this._element);
        }
    }

    // Updates the game elements
    private update(): void {
        // checks which car to update
        if (this._currentTurn == 0) {
            this._car.get("carOrange").update();
        }
        else if (this._currentTurn == 1) {
            this._car.get("carWhite").update();
        }

        this.collision();
        this._parkingSpace.update();
        this._scoreboard.update();
        this.highlightPlayer();
    }

    // handles the key down events
    private keyDownHandler = (e: KeyboardEvent): void => {
        // increases the movement rate as long as the space bar is pressed
        if (e.keyCode === 32) {
            this._carMoveRate += 50;
        }
    }

    // handles the key up events
    private keyUpHandler = (e: KeyboardEvent): void => {
        // Moves the current car when the space bar is released
        if (e.keyCode === 32) {
            // checks which car to move
            if (this._currentTurn == 0) {
                this._car.get("carOrange").move(this._carMoveRate);
            }
            if (this._currentTurn == 1) {
                this._car.get("carWhite").move(this._carMoveRate);
            }

            this._carMoveRate = 0;
            this.update();
        }

        // stops the game when ESC is pressed
        if (e.keyCode === 27) {
            this._stopGame = 1;
        }
    }

    // Game loop
    private loop = () => {
        this.collision();
        this.update();
        this.endGame();

        // checks if the game needs to be stopped
        if (this._stopGame == 0) {
            requestAnimationFrame(this.loop)
        }
        else if (this._stopGame == 1) {
            console.log('Game has stopped');
        }
    }

    // change to the next player/car
    private changePlayer(): void {
        if (this._currentTurn == 0) {

            // reset the position
            this._car.get("carOrange").positionReset();

            // remove current car
            var childImg = document.getElementById('carOrangeImg');
            var childDiv = document.getElementById('carOrange');
            childDiv.removeChild(childImg);
            this._element.removeChild(childDiv);

            // draw new car
            this._car.get("carWhite").draw(this._element);

            this._currentTurn = 1;
        }
        else if (this._currentTurn == 1) {

            // reset the position
            this._car.get("carWhite").positionReset();

            // remove current car
            var childImg = document.getElementById('carWhiteImg');
            var childDiv = document.getElementById('carWhite');
            childDiv.removeChild(childImg);
            this._element.removeChild(childDiv);

            // draw new car
            this._car.get("carOrange").draw(this._element);

            this._currentTurn = 0;
        }
    }

    // removes the highlights in the scoreboard
    private unhighlightPlayers(): void {
        var element1 = document.getElementById("p1");
        var element2 = document.getElementById("p2");

        element1.removeAttribute("style");

        element2.removeAttribute("style");
    }

    // draws the highlight in the scoreboard
    private highlightPlayer(): void {
        // unhighlights any currently highlighted players
        this.unhighlightPlayers();

        // draw the highlight for the current player
        var element: HTMLElement;
        if (this._currentTurn == 0) {
            element = document.getElementById("p1");
        }
        else if (this._currentTurn == 1) {
            element = document.getElementById("p2");
        }

        element.style.border = "2px dashed cyan";
        element.style.backgroundColor = "black";
    }

    // ends the game when a certain score has been reached
    private endGame(): void {
        var score1: Number = this._scoreboard.getScore1();
        var score2: Number = this._scoreboard.getScore2();

        if (score1 == 5 || score2 == 5) {
            this._stopGame = 1;
        }
    }

    // adds the game menu
    private gameMenu(): void {
        // creates wrapper
        var wrapper = document.createElement('div');
        wrapper.className = 'startMenu';
        wrapper.id = 'startMenu';

        // creates text
        var menuText = document.createElement('p');
        menuText.className = 'menuText';
        menuText.id = 'menuText';
        menuText.innerHTML = 'Welcome to a super hard 2-player car parking game. <br> Press START to play. <br> Game controls: <br> Hold \"space\" to accelerate. <br> Press \"ESC\" to quit.';

        // creates button
        var startButton = document.createElement('button');
        startButton.addEventListener('click', this.startGame);
        startButton.className = 'startButton';
        startButton.id = 'startButton';
        startButton.innerHTML = 'Start';

        // append elements to the container
        wrapper.appendChild(menuText);
        wrapper.appendChild(startButton);
        this._element.appendChild(wrapper);
    }

    // removes game menu
    private removeGameMenu(): void {
        // gets elements
        var parentDiv = document.getElementById('startMenu');
        var child1 = document.getElementById('menuText');
        var child2 = document.getElementById('startButton');

        // removes elements
        parentDiv.removeChild(child1);
        parentDiv.removeChild(child2);
        this._element.removeChild(parentDiv);
    }

    // when clicked on start in the game menu, start game
    private startGame = (e: MouseEvent): void => {
        if (e.button === 0 || e.button === 1 || e.button === 2) {
            // remove the game menu
            this.removeGameMenu();

            // add event listeners
            window.addEventListener('keydown', this.keyDownHandler);
            window.addEventListener('keyup', this.keyUpHandler);

            // draws the game objects and starts the game loop
            this.draw();
            this.loop();
        }
    }

}