/// <reference path="GameObject.ts" />

class ScoreBoard extends GameObject {

    // attributes
    private _score1: number;
    private _score2: number;

    // constructor
    constructor(name: string, xPosition: number = 0, yPosition: number = 0) {
        super(name, xPosition, yPosition);

        // sets score
        this._score1 = 0;
        this._score2 = 0;
    }

    // methodes
    
    // returns score1
    public getScore1(): Number {
        return this._score1;
    }

    // returns score2
    public getScore2(): Number {
        return this._score2;
    }

    // increases the score by 1
    public increaseScoreOf(player: Number): void {
        // checks which score to increase
        if(player == 0){
            this._score1 += 1;
        }
        else if(player == 1){
            this._score2 += 1;
        }
    }

    // draws the scoreboard
    public draw(container: HTMLElement): void {
        //create div
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;

        //create p
        const p0 = document.createElement('p');
        p0.innerHTML = 'The score\'s are: ';

        const p1 = document.createElement('p');
        p1.id = "p1";
        p1.innerHTML = 'Player1 = ';

        const p2 = document.createElement('p');
        p2.id = "p2";
        p2.innerHTML = 'Player2 = ';

        //create span
        const span1 = document.createElement('span');
        span1.id = 'span1';
        span1.innerHTML = this._score1.toString();

        const span2 = document.createElement('span');
        span2.id = 'span2';
        span2.innerHTML = this._score2.toString();

        //append elements
        p1.appendChild(span1);
        p2.appendChild(span2);
        this._element.appendChild(p0);
        this._element.appendChild(p1);
        this._element.appendChild(p2);
        container.appendChild(this._element);
    }

    // updates the scoreboards score
    public update(): void {
        var scoreSpan1 = document.getElementById('span1');
        var scoreSpan2 = document.getElementById('span2');
        scoreSpan1.innerHTML = this._score1.toString();
        scoreSpan2.innerHTML = this._score2.toString();
    }

}