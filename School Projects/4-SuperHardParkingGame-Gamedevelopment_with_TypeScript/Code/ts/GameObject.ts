class GameObject {

    // attributes
    protected _element: HTMLElement;
    protected _name: string;
    protected _xPos: number;
    protected _yPos: number;

    // constructor
    constructor(objectName: string, xPosition: number = 0, yPosition: number = 0) {
        // sets objects name
        this._name = objectName;

        // sets objects position
        this._xPos = xPosition;
        this._yPos = yPosition;
    }

    // methodes
    
    public set xPos(xPosition: number) {
        this._xPos = xPosition;
    }

    public set yPos(yPosition: number) {
        this._yPos = yPosition;
    }

    // resets x and y to 0
    public positionReset(): void {
        this._xPos = 0;
        this._yPos = 0;
    }

    // draws game object
    public draw(container: HTMLElement): void {
        //create div
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;

        //create image
        const image = document.createElement('img');
        image.src = `./images/${this._name}.png `;
        image.id = this._name + 'Img';

        //append elements
        this._element.appendChild(image);
        container.appendChild(this._element);
    }

    // updates objects element
    public update(): void {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}