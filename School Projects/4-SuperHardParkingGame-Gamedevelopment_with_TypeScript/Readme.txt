Super Hard Parking Game


Description:	We had to make a game with TypeScript, so i made this simple parking game. 
		This was my first time codding with typescript.

Objective:	De student bedenkt zelf zijn eigen opdracht. Mocht hij / zij er niet uitkomen dan wordt
		de opdracht door de docent gegeven. De opdracht heeft de volgende eisen:

		-        Het is een niet bestaand spel.

		-        Het spel heeft ten minste 3 goed herkenbare objecten (classes).

		-        Het spel is turned based.

		-        Het spel is voor 2 spelers (mag ook een ai-speler zijn)

		-        Het spel is leuk om te maken.