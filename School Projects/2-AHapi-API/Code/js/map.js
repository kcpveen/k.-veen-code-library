var map
var bilt

// Map API
function initMap() {
  window.bilt = { lat: 52.102, lng: 5.185 };
  window.map = new google.maps.Map(document.getElementById('map'), {
    zoom: 7,
    center: bilt
  });
}

function searchFunction() {

  infoWindowOwnLoc = new google.maps.InfoWindow;

  // Geolocation API.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindowOwnLoc.setPosition(pos);
      infoWindowOwnLoc.setContent('Uw locatie');
      infoWindowOwnLoc.open(map);
      map.setCenter(bilt);
    }, function () {
      handleLocationError(true, infoWindowOwnLoc, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindowOwnLoc, map.getCenter());
  };

  // Map Marker
  // var marker = new google.maps.Marker({
  //   position: adamCentraalStation,
  //   map: map
  // });

  $.getJSON('./json/AH_db_stores.json', function (responseData) {
    // console.log (responseData);
    responseData.forEach(function (obj) {
      // console.log(obj);
      lat = obj.lat;
      long = obj.long;
      if (lat != "" && long != "") {
        var AHMarkerImage = "./images/AH_Marker_icon.svg";
        var marker = new google.maps.Marker({
          position: { lat: parseFloat(lat), lng: parseFloat(long) },
          animation: google.maps.Animation.DROP,
          title: (obj.StoreName + " " + obj.street + " " + obj.housenr + " " + obj.city),
          icon: AHMarkerImage,
          map: map

        });
        if (marker.addListener('click', function () {
          infoWindow = new google.maps.InfoWindow;
          infoWindow.setContent(obj.StoreName + " " + obj.street + " " + obj.housenr + " " + obj.city);
          infoWindow.open(map, marker);

          addHTMLStoreInfo();

          function addHTMLStoreInfo() {
            var input1 = "<h4> De geselecteerde Albert Heijn: </h4>" + obj.StoreName + "<br />" + obj.street + " " + obj.housenr + "<br />" + obj.city + "<br />" + obj.country + "<br />" + "<br />";
            var input2 = "<h5> Winkelservices: </h5>"  + "<br />";
            var input3 = "<h5> Openingstijdeninfo: </h5>" + obj.status + "<br />";
            var input4 = "<br />" + "<br />"  + "<br />"; 
            var input5 = "<h5> Contact: </h5>" +"<br /> " +  "<br />" + obj.phoneNumber + "<br />"; 

            for (key in obj) {
              if (key.startsWith("service_")) {
                input2 = input2 + "<li>"+ obj[key] + "</li>";
              }
            }

            for (key in obj) {
              if (key.startsWith("sunday")) {
                input3 = input3 + obj[key] + "<br/>";
              }
            }

            for (key in obj) {
              if (key.startsWith("openEvening")) {
                input3 = input3+ obj[key] + "<br />" ;
              }
            }

            var text1 = document.getElementById("storeInfo");
            text1.innerHTML = input1 + input2;
            var text2= document.getElementById("openingstijden");
            text2.innerHTML = input3 + input4 + input5;
          }
        }));
      }
    })

  });
}


function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
    'Error: The Geolocation service failed.' :
    'Error: Your browser doesn\'t support geolocation.');
  infoWindow.open(map);
}

