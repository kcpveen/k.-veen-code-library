var pictureArray = ['dog', 'cat', 'frog', 'mouse', 'bug', 'fly', 'spider', 'bat', 'monkey'];
var pictureToGuess = randomPicture();

window.addEventListener("load", init);

function init() {
    showPictures();
    showPictureToGuess();
};


function showPictures() {

    // <img src="cat.jpg" id='cat' class='img--small' />

    for (var index in pictureArray) {
        var img = document.createElement('img');
        img.setAttribute('src', 'images/' + pictureArray[index] + '.jpg');
        img.setAttribute('class', 'img--small');
        img.setAttribute('id', pictureArray[index]);
        img.addEventListener('click', handleImgClickEvent);
        var ph = document.getElementById('img-grid');
        ph.appendChild(img);
    }
}

function handleImgClickEvent(event){
    console.log("clicked: " + event.target.id);
    var chosenPicture = event.target.id;
    if (chosenPicture == pictureToGuess){
        console.log('Good job, that is correct!!!');
        document.getElementById('message').innerHTML = 'Good job, that is correct!!!';
        pictureToGuess = randomPicture();
        showPictureToGuess();
    }else{
        console.log('Too bad, refresh and try again.');
        document.getElementById('message').innerHTML = 'Too bad, try again.';
    }
    }

function showPictureToGuess() {
    var img = document.getElementById('random-img');
    img.setAttribute('src', 'images/' + pictureToGuess + '.jpg')
}

function randomPicture(){
    return pictureArray[Math.floor(pictureArray.length * Math.random())];
}