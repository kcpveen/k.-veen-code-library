/**
 * Write a description of class item here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Item
{
    private String name;
    private String description;
    private int weight;
    private int value;
    
    /**
     * Constructor for a item.
     * A item needs a name, description, weight and a value.
     */
    public Item(String itemName, String itemDescription, int itemWeight, int value)
    {
        name = itemName;
        description = itemDescription;
        weight = itemWeight;
        this.value = value;
    }

    /**
     * Returns the item's Description.
     */
    public String getItemDescription()
    {
        return description;
    }
    
    /**
     * Returns the item's weight.
     */
    public int getItemWeight()
    {
        return weight;
    }
    
    /**
     * returns the item's name.
     */
    public String getItemName()
    {
        return name;
    }
    
    /**
     * returns a full description of the item.
     */
    public String fullItemDescription()
    {
        return getItemName() + ": \"" + getItemDescription() + "\"" + " (" + getItemWeight() + ".Kg)" + ".\n";
    }
    
    /**
     * returns the item's value.
     */
    public int getValue()
    {
        return value;
    }
}
