import java.util.Stack;
import java.util.HashMap;
/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.07.31
 */

public class Game 
{
    private Parser parser;
    private Stack<Room> lastRooms;
    private Player player;
    private int moves;
    private HashMap<String, Room> rooms;

    /**
     * Create the game, game world and player.
     */
    public Game(String playerName) 
    {
        player = new Player(playerName);
        rooms = new HashMap<>();
        createWorld();
        parser = new Parser();
        lastRooms = new Stack<Room>();
        moves = 10;
    }
    
    /**
     * Static main method for running the game without blueJ.
     */
    public static void main (String[] args)
    {
        Game game = new Game("admin");
        game.play();
    }

    /**
     * Creates the game world.
     */
    private void createWorld()
    {
        Room noobForest, southrenVillage, dragonRoad, dragonsDen, southrenTeleporter, forestEntrance, mysteriousHole, eastrenForest, darkWoods, trollsLair, forestShack, forestLake, westrenCity;
        Room westrenTeleporter, river, waterfall, witchesHut, goldMines, mountainFoot, mountainTop, northrenCapital, northrenTeleporter, teleporterRoom, shop;

        // create the rooms
        noobForest = new Room("in the epic starter forest for noobs.", null);
        rooms.put("noobForest", noobForest);

        southrenVillage = new Room("in the southren village.", null);
        rooms.put("southrenVillage", southrenVillage);

        dragonRoad = new Room("on the dragon\'s road", "key");
        rooms.put("dragonRoad", dragonRoad);

        dragonsDen = new Room("in the dragon\'s den.", null);
        rooms.put("dragonsDen", dragonsDen);

        southrenTeleporter = new Room("at the southren teleporter stone. \n north -> northren teleporter stone \n west -> westren teleporter stone", "key");
        rooms.put("southrenTeleporter", southrenTeleporter);

        forestEntrance = new Room("at the forest entrance.", null);
        rooms.put("forestEntrance", forestEntrance);

        mysteriousHole = new Room("at the mysterious hole \n in a flash of light you are killed and dropped in the hole.", null);
        rooms.put("mysteriousHole", mysteriousHole);

        eastrenForest = new Room("in the eastren forest.", null);
        rooms.put("eastrenForest", eastrenForest);

        darkWoods = new Room("in the dark woods.", null);
        rooms.put("darkWoods", darkWoods);

        trollsLair = new Room("in the troll\'s lair.", null);
        rooms.put("trollsLair", trollsLair);

        forestShack = new Room("at the forest shack.", null);
        rooms.put("forestShack", forestShack);

        forestLake = new Room("at the forest lake.", null);
        rooms.put("forestLake", forestLake);

        westrenCity = new Room("in the westren city.", null);
        rooms.put("westrenCity", westrenCity);

        westrenTeleporter = new Room("at the westren teleporter stone. \n north -> northren teleporter stone \n south -> southren teleporter stone", "key");
        rooms.put("westrenTeleporter", westrenTeleporter);

        river = new Room("at the river.", null);
        rooms.put("river", river);

        waterfall = new Room("at the waterfalls.", null);
        rooms.put("waterfall", waterfall);

        witchesHut = new Room("at the witches hut.", null);
        rooms.put("witchesHut", witchesHut);

        goldMines = new Room("in the gold mines.", null);
        rooms.put("goldMines", goldMines);

        mountainFoot = new Room("at the mountain\'s foot.", null);
        rooms.put("mountainFoot", mountainFoot);

        mountainTop = new Room("on top of the mountain.", null);
        rooms.put("mountainTop", mountainTop);

        northrenCapital = new Room("in the northren capital city.", null);
        rooms.put("northrenCapital", northrenCapital);

        northrenTeleporter = new Room("at the northren teleporter stone. \n south -> southren teleporter stone \n west -> westren teleporter stone", null);
        rooms.put("northrenTeleporter", northrenTeleporter);

        teleporterRoom = new Room("in the universal alien teleporter room.\n(use: teleport \"destination\") \nChoose your destination: \n", null);
        rooms.put("teleporterRoom", teleporterRoom);
        
        shop = new Room("in a shop full of goodies. \nType trade to start trading.", null);
        rooms.put("shop", shop);

        // initialise room exits

        noobForest.setExits("north", southrenVillage);
        noobForest.setExits("west", dragonRoad);

        southrenVillage.setExits("west", southrenTeleporter);
        southrenVillage.setExits("north", forestEntrance);
        southrenVillage.setExits("south", noobForest);
        southrenVillage.setExits("shop", shop);
        
        dragonRoad.setExits("east", noobForest);
        dragonRoad.setExits("west", dragonsDen);

        dragonsDen.setExits("east", dragonRoad);

        southrenTeleporter.setExits("north", northrenTeleporter);
        southrenTeleporter.setExits("east", southrenVillage);
        southrenTeleporter.setExits("west", westrenTeleporter);

        forestEntrance.setExits("north", forestLake);
        forestEntrance.setExits("east", eastrenForest);
        forestEntrance.setExits("south", southrenVillage);
        forestEntrance.setExits("west", mysteriousHole);

        eastrenForest.setExits("north", forestShack);
        eastrenForest.setExits("east", mysteriousHole);
        eastrenForest.setExits("south", darkWoods);
        eastrenForest.setExits("west", forestEntrance);

        darkWoods.setExits("north", eastrenForest);
        darkWoods.setExits("east", mysteriousHole);
        darkWoods.setExits("south", trollsLair);
        darkWoods.setExits("west", mysteriousHole);
        
        trollsLair.setExits("north", darkWoods);

        forestShack.setExits("south", eastrenForest);
        forestShack.setExits("west", forestLake);

        forestLake.setExits("north", river);
        forestLake.setExits("east", forestShack);
        forestLake.setExits("south", forestEntrance);
        forestLake.setExits("west", westrenCity);

        westrenCity.setExits("east", forestLake);
        westrenCity.setExits("west", westrenTeleporter);

        westrenTeleporter.setExits("north", northrenTeleporter);
        westrenTeleporter.setExits("east", westrenCity);
        westrenTeleporter.setExits("south", southrenTeleporter);

        river.setExits("east", mountainFoot);
        river.setExits("south", forestLake);
        river.setExits("west", waterfall);

        waterfall.setExits("north", goldMines);
        waterfall.setExits("east", river);
        waterfall.setExits("west", witchesHut);

        witchesHut.setExits("north", mysteriousHole);
        witchesHut.setExits("east", waterfall);

        goldMines.setExits("south", waterfall);

        mountainFoot.setExits("north", mountainTop);
        mountainFoot.setExits("east", mysteriousHole);
        mountainFoot.setExits("west", river);

        mountainTop.setExits("east", mysteriousHole);
        mountainTop.setExits("south", mountainFoot);
        mountainTop.setExits("west", northrenCapital);

        northrenCapital.setExits("east", mountainTop);
        northrenCapital.setExits("west", northrenTeleporter);

        northrenTeleporter.setExits("east", northrenCapital);
        northrenTeleporter.setExits("south", southrenTeleporter);
        northrenTeleporter.setExits("west", westrenTeleporter);

        // initialise room items

        noobForest.addNewItem("chest", "a loot chest ful of suprises.", 1, 0);

        trollsLair.addNewItem("staff", "fire staff, infused with fire magic", 3, 75);
        trollsLair.addNewItem("crystal", "Strong magic resonates from this crystal (max carrying weight +10 .kg", 1, 100);
        
        dragonsDen.addNewItem("teleporter", "a alien teleporting deivce", 1, 10000);
        
        northrenTeleporter.addNewItem("key", "a key, for what could it be?.", 1, 0);
        
        // initialise room npcs
        
        southrenVillage.addNpc("peddler");
        
        westrenCity.addNpc("peddler");
        
        forestShack.addNpc("Hunter Bill");
        
        river.addNpc("lone wanderer micheal");
        
        witchesHut.addNpc("witch Maria");
        
        northrenCapital.addNpc("peddler");
        
        //possition the player in the starting Room:
        player.setCurrentRoom(noobForest);
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        String endText = "";
        boolean finished = false;
        while (!finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
            if(moves <= 0)
            {
                endText = "\n   GAME OVER \nOut of moves!! \n";
                finished = true;
            }
        }
        System.out.println(endText + "Thank you for playing.  Good bye.");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("Welcome to the Wizard Kingdomm!");
        System.out.println("The World of the Wizard Kingdom is a mysteriously boring adventure game.");
        System.out.println("Type 'help' if you need help.");
        System.out.println();
        System.out.println("moves left: " + getMoves() + player.getCurrentRoom().getLongDescription());
        System.out.println();
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        if(command.isUnknown()) {
            System.out.println("I don't know what you mean...");
            return false;
        }

        String commandWord = command.getCommandWord();
        if (commandWord.equals("help")) {
            printHelp();
        }
        else if (commandWord.equals("go"))
        {
            goRoom(command);
        }
        else if (commandWord.equals("look"))
        {
            look();
        }
        else if (commandWord.equals("use"))
        {
            use(command);
        }
        else if (commandWord.equals("back"))
        {
            goBack();
        }
        else if (commandWord.equals("take"))
        {
            pickUpItem(command);
        }
        else if (commandWord.equals("drop"))
        {
            dropItem(command);
        }
        else if (commandWord.equals("inventory"))
        {
            listInventory();
        }
        else if (commandWord.equals("teleport"))
        {
            teleportTo(command);
        }
        else if (commandWord.equals("talk"))
        {
            talkTo(command);
        }
        else if (commandWord.equals("quit"))
        {
            wantToQuit = quit(command);
        }

        return wantToQuit;
    }

    /**
     * Returns the amount of moves left.
     */
    private int getMoves()
    {
        return moves;
    }

    /**
     * Returns a certain room object.
     */
    private Room getRoom(String roomName)
    {
        return rooms.get(roomName);
    }

    /**
     * List all the rooms in the world.
     */
    private String listRooms()
    {
        String roomList = "";
        for(String rooms : rooms.keySet())
        {
            roomList += "-" + rooms + "\n";
        }
        return roomList;
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        System.out.println("You are lost. You are alone. You wander");
        System.out.println("around at the university.");
        System.out.println();
        System.out.println("Your command words are:");
        System.out.println(parser.getCommandList());
    }

    /** 
     * Try to go in one direction. If there is an exit and does not require a key Item, enter
     * the new room, otherwise print an error message.
     */
    private void goRoom(Command command) 
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Go where?");
            return;
        }

        String direction = command.getSecondWord();

        // Try to leave current room.
        Room nextRoom = player.getCurrentRoom().getExit(direction);

        //check if room requires a key
        if(nextRoom.getKey() == null)
        {
            if (nextRoom == null) {
                    System.out.println("There is no door!");
                }
                else if(nextRoom != null)
                {
                    moves -= 1;
                    lastRooms.push(player.getCurrentRoom());
                    player.setCurrentRoom(nextRoom);
                    System.out.println("moves left: " + getMoves() + player.getCurrentRoom().getLongDescription());
                }
        }
        
        if(nextRoom.getKey() == "key")
        {
            if(player.inventoryCheck("key"))
            {
                if (nextRoom == null) {
                    System.out.println("There is no door!");
                }
                else if(nextRoom != null)
                {
                    moves -= 1;
                    lastRooms.push(player.getCurrentRoom());
                    player.setCurrentRoom(nextRoom);
                    System.out.println("moves left: " + getMoves() + player.getCurrentRoom().getLongDescription());
                }
            }
            else
            {
                System.out.println("You can not yet enter, you need a item for that..");
            }
        }
    }
    
    /**
     * Go back to the previous room, until there are no more rooms left.
     */
    private void goBack()
    {
        if(lastRooms.empty() != true)
        {
            player.setCurrentRoom(lastRooms.pop());
            moves -= 1;
            System.out.println("moves left: " + getMoves() + player.getCurrentRoom().getLongDescription());
        }
        else
        {
            System.out.println("There is nothing to go back to.");
        }
    }

    /**
     * prints the longdescription from the current room.
     */
    private void look()
    {
        System.out.println(player.getCurrentRoom().getLongDescription());
    }

    /**
     * Use a item from the players inventory.
     */
    private void use(Command command)
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Use what?");
            return;
        }

        String secCommand = command.getSecondWord();

        if(player.inventoryCheck(secCommand))
        {
            boolean finished = false;
            if(secCommand.equals("crystal"))
            {
                player.removeItemFromInventory(secCommand);
                int playerMaxWeight = player.getMaxCarryingWeight();
                playerMaxWeight += 5;
                player.setMaxCarryingWeight(playerMaxWeight);
                System.out.println("max carrying weight +10");
            }
            else if(secCommand.equals("teleporter"))
            {
                player.setCurrentRoom(getRoom("teleporterRoom"));
                System.out.println("You are " + player.getCurrentRoom().getDescription() + "\n" + listRooms());
            }
            else if(secCommand.equals("chest"))
            {
                player.increaseGold(100);
                System.out.println("+100 gold");
            }
        }
        else
        {
            System.out.println("Not in inventory");
        }
    }
    
    /**
     * picks a item up from a Room object.
     */
    private void pickUpItem(Command command)
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Take what?");
            return;
        }

        String itemName = command.getSecondWord();

        Item pickedUpItem = player.getCurrentRoom().getRoomItem(itemName);
        player.addItemToInventory(itemName, pickedUpItem);
    }

    /**
     * drops a item from the players inventory in the current room object.
     */
    private void dropItem(Command command)
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Drop what?");
            return;
        }

        String itemName = command.getSecondWord();

        Item discardedItem  = player.getInventoryItem(itemName);
        player.getCurrentRoom().addItem(itemName, discardedItem);
        System.out.println(itemName + " was dropped!!");
    }

    /**
     * list's all items from the players inventory.
     */
    private void listInventory()
    {
        System.out.println(player.listInventory());
    }

    /**
     * If the teleporter item is in the players inventory, teleport to any listed room.
     */
    private void teleportTo(Command command)
    {
        if(player.inventoryCheck("teleporter"))
        {
            if(!command.hasSecondWord()) {
                // if there is no second word, we don't know where to go...
                System.out.println("teleport to where?");
                return;
            }

            String locationName = command.getSecondWord();
            player.setCurrentRoom(getRoom(locationName));
            System.out.println(player.getCurrentRoom().getLongDescription());
        }
        else
        {
            System.out.println("Unknown command!!");
        }
    }
    
    /**
     * talk to a npc object.
     */
    private void talkTo(Command command)
    {
        String npcName = command.getSecondWord();
        Npc npc = player.getCurrentRoom().getNpc(npcName);
        if(npc.getName().equals("peddler"))
        {
            npc.shopInterface();
        }
        else
        {
            npc.talk();
        }
    }

    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }
}
