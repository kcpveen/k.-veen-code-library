import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
/**
 * Class Room - a room in an adventure game.
 *
 * This class is part of the "World of Zuul" application. 
 * "the world of the wizard kingdom" is a very simple, text based adventure game.  
 *
 * A "Room" represents one location in the scenery of the game.  It is 
 * connected to other rooms via exits.  The exits are labelled north, 
 * east, south, west.  For each direction, the room stores a reference
 * to the neighboring room, or null if there is no exit in that direction.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.07.31
 */
public class Room 
{
    private String description;
    private HashMap<String, Room> exits;
    private HashMap<String, Item> items;
    private String key;
    private HashMap<String, Npc> npcs;

    /**
     * Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * also define a key if the room needs to have a key, else the key is null.
     * @param description The room's description.
     */
    public Room(String description, String keyName) 
    {
        this.description = description;
        exits = new HashMap<String, Room>();
        items = new HashMap<>();
        npcs = new HashMap<>();;
        if(keyName != null)
        {
            key = keyName;
        }
        else
        {
            key = null;
        }
    }

    /**
     * Return the room that is reached if we go from this room in direction
     * "direction". If there is no room in that direction, return null.
     * @param direction The exit's direction.
     * @return The room in the given direction.
     */
    public Room getExit(String direction)
    {
        return exits.get(direction);
    }

    /**
     * Return a string describing the room's exits, for example
     * "Exits: north west".
     * @return Details of the room's exits.
     */
    public String getExitString()
    {
        String returnString = "Exits:";
        Set<String> keys = exits.keySet();

        for(String exit : keys)
        {
            returnString += " " + exit;
        }

        return returnString;
    }

    /**
     * @return The description of the room.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * returns all item's in a room with the item's full description.
     */
    public String getAllItemsDescription()
    {
        String allItemsDescription = "";

        for(Item values : items.values())
        {
            allItemsDescription = allItemsDescription + values.fullItemDescription();
        }

        return allItemsDescription;
    }

    /**
     * Return a description of the room in the form:
     *     You are in the kitchen.
     *     Exits: north west
     * @return A long description of this room
     */
    public String getLongDescription()
    {
        String messageString = "\n\nYou are " + description + "\n \n" + "You see: " + "\n";
        if(items.isEmpty() != true)
        {
            messageString += getAllItemsDescription();
        }
        else
        {
            messageString += "Nothing of intrest.";
        }
        if(npcs.isEmpty() != true)
        {
            messageString += "\nPeople around: \n" + listNpcs();
        }
        return messageString + "\n \n" + getExitString();
    }

    /**
     * returns a certain item in the room.
     */
    public Item getRoomItem(String itemName)
    {
        Item item = items.get(itemName);
        items.remove(itemName);
        return item;
    }

    /**
     * returns the key from the room.
     */
    public String getKey()
    {
        return key;
    }

    /**
     * returns a certain npc object.
     */
    public Npc getNpc(String npcName)
    {
        return npcs.get(npcName);
    }

    /**
     * list's all npc's in a room.
     */
    public String listNpcs()
    {
        String allNpcsDescription = "";

        for(Npc values : npcs.values())
        {
            allNpcsDescription += values.getName();
        }

        return allNpcsDescription;
    }

    /**
     * Define the exits of this room.  Every direction either leads
     * to another room or is null (no exit there).
     * @param north The north exit.
     * @param east The east east.
     * @param south The south exit.
     * @param west The west exit.
     */
    public void setExits(String direction, Room neighbor) 
    {
        exits.put(direction, neighbor);
    }

    /**
     * Add's a new item object to the room.
     */
    public void addNewItem(String itemName, String description, int weight, int value)
    {
        items.put(itemName, new Item(itemName, description, weight, value));
    }

    /**
     * add's a existing item object to the room.
     */
    public void addItem(String itemName, Item item)
    {
        items.put(itemName, item);
    }

    /**
     * add's a new npc object to the room.
     */
    public void addNpc(String npcName)
    {
        npcs.put(npcName, new Npc(npcName));
    }
}
