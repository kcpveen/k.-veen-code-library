import java.util.HashMap;
/**
 * Write a description of class Player here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Player
{
    private String playerName;
    private Room currentRoom;
    private HashMap<String, Item> inventory; 
    private int maxCarryingWeight;
    private int currentWeight;
    private int gold;

    /**
     * Constructor for Player.
     * A player need a name.
     */
    public Player(String playerName)
    {
        playerName = playerName;
        currentRoom = null;
        inventory = new HashMap<>();
        maxCarryingWeight = 5;
        currentWeight = 0;
        gold = 0;
    }

    /**
     * returns the room the player is in.
     */
    public Room getCurrentRoom()
    {
        return currentRoom;
    }

    /**
     * returns a certain item from the player's inventory.
     */
    public Item getInventoryItem(String itemName)
    {
        Item item = inventory.get(itemName);
        removeItemFromInventory(itemName);
        return item;
    }
    
    /**
     * removes a certain item from the player's inventory.
     */
    public void removeItemFromInventory(String itemName)
    {
        Item item = inventory.get(itemName);
        currentWeight -= item.getItemWeight();
        inventory.remove(itemName);
    }
    
    /**
     * returns the maximum weight a player can carry.
     */
    public int getMaxCarryingWeight()
    {
        return maxCarryingWeight;
    }

    /**
     * returns a string with all items in the player's inventory.
     */
    public String listInventory()
    {
        String itemList = "";
        for(String key : inventory.keySet())
        {
            String item = inventory.get(key).fullItemDescription();
            itemList += " -" + item;
        }
        return "Your inventory: \n" + itemList + listWeight() + "\nGold: " + getGold();
    }
    
    /**
     * returns a string with the players current carrying weight.
     */
    private String listWeight()
    {
        return "\nWeight: " + currentWeight + ".Kg" + " Max carrying capacity: " + maxCarryingWeight + ".Kg";
    }
    
    /**
     * checks if a certain item exists in the players inventory.
     */
    public boolean inventoryCheck(String itemToCheck)
    {
        boolean returnStatement = false;
        if(inventory.containsKey(itemToCheck))
        {
            returnStatement = true;
        }
        return returnStatement;
    }
    
    /**
     * returns the amount of gold a player has.
     */
    public int getGold()
    {
        return gold;
    }

    /**
     * sets the current room the player is in.
     */
    public void setCurrentRoom(Room currentRoom)
    {
        this.currentRoom = currentRoom;
    }

    /**
     * checks current player weight against the item's weight and then add's the item to the inventory.
     */
    public void addItemToInventory(String itemName, Item item)
    {
        if(item.getItemWeight() + currentWeight <= maxCarryingWeight)
        {
            inventory.put(itemName, item);
            currentWeight += item.getItemWeight();
            System.out.println(itemName + " was added to your inventory.");
        }
        else
        {
            System.out.println("You can not carry this.");
        }
    }
    
    /**
     * sets the players maximum carrying weight.
     */
    public void setMaxCarryingWeight(int maxCarryingWeight)
    {
        this.maxCarryingWeight = maxCarryingWeight;
    }
    
    /**
     * increase the players gold by a certain amount.
     */
    public void increaseGold(int amount)
    {
        gold += amount;
    }
    
    /**
     * decreases the players gold by a certain amount.
     */
    public void decreaseGold(int amount)
    {
        gold -= amount;
    }
}
