The Wizard Kingdom


Description:	This is my java game that we had to make in BlueJ. This was my first attempt at java coding.
		The game itself is an old school text based adventure game, based on the world of zuul.

Objective:	Het spel �The World of Zuul� is een text-based adventure game. In zijn beginvorm is het spel 
		erg eenvoudig en niet compleet � het is jullie taak om het spel uit te breiden en verder te 
		ontwikkelen, aan de hand van de oefeningen uit hoofdstuk 8 van het BlueJ-boek, de theorie uit 
		de andere bestudeerde hoofstukken en je eigen fantasie.